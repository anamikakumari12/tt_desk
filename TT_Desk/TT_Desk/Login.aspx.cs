﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBO;
using TTDeskBL;
using OtpNet;
using System.Net.Mail;
using System.Configuration;

namespace TT_Desk
{
    
    public partial class Login : System.Web.UI.Page
    {
        #region GlobalDeclarations
        CommonFunctions objCom = new CommonFunctions();
        AuthClass authObj = new AuthClass();
        static string OTP = null;
        static string StrOtp = null;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["UserFullName"] = null;
                Session["LoginMailId"] = null;
            }
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// Desc: On click of login button it will validate the given credential and logon to application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            LoginBO objLoginBO;
            LoginBL objLoginBL;
            string scriptString = "";
            try
            {
                objLoginBL = new LoginBL();
                objLoginBO = new LoginBO();
                objLoginBO.emailid = Convert.ToString(txtemail.Text);
                objLoginBO.password = objCom.Encrypt(Convert.ToString(txtPassword.Text));
                //Session["LoggedInPassword"] = Convert.ToString(txtPassword.Text);
                objLoginBO = objLoginBL.deskLoginBL(objLoginBO);
                if (objLoginBO.Error_code == 0)
                {
                    StrOtp = GenerateOtp();
                    string otp = Convert.ToString(Session["otp"]);
                    authObj.LoginMailID = Convert.ToString(txtemail.Text);
                    //authObj.LoginMailID = authObj.ChangeMailId(authObj);
                    Session["authObj"] = authObj;
                    SendOtpToMail(StrOtp, authObj.LoginMailID);
                    Session["UserFullName"] = objLoginBO.FullName;
                    Session["LoginMailId"] = objLoginBO.emailid;
                    Session["UserId"] = objLoginBO.UserId;
                   
                    //Response.Redirect("PendingTasks.aspx");
                    Response.Redirect("Otp.aspx");
                }
                else if (objLoginBO.Error_code == 1)
                {
                    scriptString = "<script type='text/javascript'> alert('There is some error in authentication, Please try again.');</script>";
                    
                }
                else if (objLoginBO.Error_code>1)
                {
                    scriptString = "<script type='text/javascript'> alert('"+objLoginBO.Error_msg+"');</script>";
                    
                }
                ClientScriptManager script = Page.ClientScript;
                script.RegisterClientScriptBlock(GetType(), "Script", scriptString);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                scriptString = null;
            }
            
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 30th July 2020
        /// Desc: On click of request password, Reset the password into db and sent the new password in registered mail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkRequest_Click(object sender, EventArgs e)
        {
            string result = string.Empty;
            CommonFunctions comobj = new CommonFunctions();
            try
            {
                ResetBO objResetBo = new ResetBO();
                LoginBO objLoginBO = new LoginBO();
                LoginBL objLoginBL = new LoginBL();
                objLoginBO.emailid = Convert.ToString(txtemail.Text);
                objResetBo.Engid = Convert.ToString(txtemail.Text);
              
                //to create a random password
                string password = CreateRandomPassword(8);
                objLoginBO.password = comobj.Encrypt(password);
                objResetBo.Newpassword = comobj.Encrypt(password);
                objResetBo.OldPassword = "";
                // save the encrypted password to database
                objResetBo = objLoginBL.ChangePasswordBL(objResetBo);
                if (objResetBo.Error_msg.Equals("Success"))
                {
                    //
                    if (!String.IsNullOrEmpty(objLoginBO.emailid))
                    {
                        SendMail(password, objLoginBO.emailid);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Password has been reset and sent to your mail ID.');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Password has been reset but your mail ID is not registerred to the system. Please contact administrator for new password.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + objLoginBO.Error_msg + " Please enter valid Emailis and try again.');", true);
                }
            }
            catch (Exception ex)
            {
                comobj.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Authoe: Monika M S
        /// Date: 30th July 2020
        /// Desc: Create random password
        /// </summary>
        /// <param name="PasswordLength"></param>
        /// <returns>return password as a string</returns>
        private string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 30th July 2020
        /// Desc: Send new password in mail
        /// </summary>
        /// <param name="password"></param>
        /// <param name="emailTo"></param>
        private void SendMail(string password, string emailTo)
        {
            CommonFunctions comobj = new CommonFunctions();
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            EmailDetails objEmail;
            try
            {
                objEmail = new EmailDetails();
                objEmail.body = "Password has been reset for your account. <br/><br/>New Password: " + password;
                objEmail.toMailId = emailTo;
                objEmail.subject = "tt_Desk : Password Change";//Subject for your request

                comobj.SendMail(objEmail);
            }
            catch (Exception ex)
            {
                comobj.ErrorLog(ex);
            }
            finally
            {
                objEmail = new EmailDetails();
            }
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// Desc: To generate new OTP
        /// </summary>
        /// <returns>it will return OTP as a string</returns>
        public string GenerateOtp()
        {
            try
            {
                byte[] b = new byte[] { Convert.ToByte(20), Convert.ToByte('A'), Convert.ToByte('T') };
                var otp = new Totp(b, mode: OtpHashMode.Sha512, step: 5);
                OTP = otp.ComputeTotp(DateTime.UtcNow);
                Session["otp"] = OTP;
                return OTP;
            }
            catch (Exception ex)
            {
                //CommonFunctions.LogErrorStatic(ex);
                return null;
            }
            finally
            {
                OTP = null;
            }
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// Desc: It will send the OTP to mail
        /// </summary>
        /// <returns></returns>
        private void SendOtpToMail(string StrOtp, string loginmailId)
        {
            CommonFunctions comobj = new CommonFunctions();
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            EmailDetails objEmail;
            try
            {
                objEmail = new EmailDetails();
                objEmail.body = "<br/><br/>" + StrOtp + Convert.ToString(ConfigurationManager.AppSettings["OtpMessage"]) + "<br/><br/>";
                objEmail.toMailId = loginmailId;
                objEmail.subject = "tt_Desk Login OTP";//Subject for your request

                comobj.SendMail(objEmail);
            }
            catch (Exception ex)
            {
                comobj.ErrorLog(ex);
            }
            finally
            {
                objEmail = new EmailDetails();
            }

        }
    }
}