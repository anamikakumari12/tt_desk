﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteSummary.aspx.cs" Inherits="TT_Desk.QuoteSummary" MasterPageFile="~/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <link href="vendor/daterangepicker2/daterangepicker.css" rel="stylesheet" />
    <script src="vendor/daterangepicker2/daterangepicker.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <%--<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>--%>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css" />
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>

    <style>
        .table.dataTable.no-footer
        {
            width:100%!important;
        }
        .btn
        {
            font-size: 13px;
        }

        table td
        {
            border: none;
        }

        th
        {
            border: none;
        }

        thead
        {
            color: black!important;
        }

        table
        {
            border: none;
        }

        .required:after
        {
            content: " *";
            color: red;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            debugger;
            var SelectedStart = sessionStorage.getItem("selectedSummaryStart");
            var SelectedEnd = sessionStorage.getItem("selectedSummaryEnd");
            var start = (SelectedStart == null ? "07/20/2019" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#body_txtDateRange').daterangepicker({
                //opens: 'left',
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedSummaryStart', start);
                sessionStorage.setItem('selectedSummaryEnd', end);
            });

            LoadTable();
        });

        function LoadTable() {
            var head_content = $('#body_grdQuoteSummary tr:first').html();
            $('#body_grdQuoteSummary').prepend('<thead></thead>')
            $('#body_grdQuoteSummary thead').html('<tr>' + head_content + '</tr>');
            $('#body_grdQuoteSummary tbody tr:first').hide();
            var table = $('#body_grdQuoteSummary').dataTable({
                scrollY: "350px",
                scrollX: true,
                scrollCollapse: true,
                "order": [[5, 'desc']],
                //fixedColumns: true,
                //fixedColumns: {
                //    leftColumns: 1,
                //    rightColumns: 4
                //},
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                dom: 'lBfrtip',

            });

            var table = $('#body_grdQuoteSummary').DataTable();
            var divdetail = document.getElementById("divdetail");
            $('#body_grdQuoteSummary tbody').on('click', 'td:first-child .link', function () {
                debugger;
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                if (row.child.isShown()) {
                    divdetail.style.display = "none";
                    row.child.hide();
                    tr.removeClass('shown');
                } else {

                    divdetail.style.display = "block";
                    //var param = tr.context.innerText;
                    //var param = "RN_1037_2019-07-22_2";
                    var param = tr[0].cells[0].innerText;
                    console.log(param);
                    $.ajax({
                        url: 'QuoteSummary.aspx/LoadDetailedGrid',
                        method: 'post',
                        datatype: 'json',
                        data: '{ref_no:"' + param + '"}',
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            LoadData(msg);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText);
                        }
                    });

                    // Open row.
                    row.child(divdetail).show();
                    //tr.addClass('shown');
                }
            });
        }

        function LoadData(msg) {
            debugger;
            console.log(msg);
            console.log(msg.d);
            msg = JSON.parse(msg.d);
            $('#grdDetailedQuoteSummary tbody').html("");
            for (var i = 0; i < msg.length; i++) {
                var button;
                //var MOQ;
                //var OfferPrice;
                //var comment;
                //var expiry;
                //if (msg[i].Status == "Sent For Approval" && msg[i].Assigned_To == "DESK") {
                //    //if (msg[i].Accept_Visible_Flag == "1")
                //        button = "<input type=\"button\"  class=\"btn btn-success button pull-right\"  value=\"Approve\" id=\"btnApprove\"  onclick=\"return Approve('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "','Approved');\" />" +
                //        "<input type=\"button\"  class=\"btn btn-danger button pull-right\"  value=\"Reject\" id=\"btnReject\"  onclick=\"return Reject('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "');\" />";

                //    //else
                //    //    button = "<input type=\"button\"  class=\"btn btn-success button pull-right\" disabled=\"disabled\" value=\"Approve\" id=\"btnApprove\"  onclick=\"return Approve('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "','Approved');\" />" +
                //    //       "<input type=\"button\"  class=\"btn btn-danger button pull-right\"  value=\"Reject\" id=\"btnReject\"  onclick=\"return Reject('" + msg[i].ID + "', '" + msg[i].Item_code + "', '" + msg[i].Ref_number + "', '" + i + "');\" />";

                //    //+"</td>  <td>";
                //    console.log(button);
                //    MOQ = "<input type=\"text\" id=\"txtMOQ_" + i + "\" onkeypress=\"return isNumberKey(event,this);\" value=\"" + msg[i].MOQ + "\" style=\"width: 80px;\"/>";
                //    OfferPrice = "<input type=\"text\" disabled=\"disabled\" value=\"" + msg[i].Offer_Price + "\" id=\"txtOfferPrice_" + i + "\" onkeypress=\"return isNumberKey(event,this);\" style=\"width: 80px;\"/>";
                //    NewOfferPrice = "<input type=\"text\" value=\"" + msg[i].New_OfferPrice + "\" id=\"txtNewOfferPrice_" + i + "\" onkeypress=\"return isNumberKey(event,this);\" onkeyup=\"return setTextareaVal(event,this);\" style=\"width: 80px;\"/><br/><span id=\"lblpricemsg_" + i + "\" style=\"color:red;\"></span>";
                //    comment = "<textarea  id=\"txtComment_" + i + "\" cols=\"20\" rows=\"2\"  onkeyup=\"return setTextareaVal(event,this);\"></textarea><br/><span id=\"lblmsg_" + i + "\" style=\"color:red;\"></span>";
                //   // expiry = "<input type=\"text\" id=\"txtExpiryDate_" + i + "\"><input type=\"hidden\" id=\"hdnExpiryDate_" + i + "\" value=\"" + msg[i].ExpiryDate + "\">"
                //}
                //else {
                //    button = "</td>  <td>";

                //    MOQ = "<input type=\"text\" disabled=\"disabled\" id=\"txtMOQ_" + i + "\" onkeypress=\"return isNumberKey(event,this);\" value=\"" + msg[i].MOQ + "\" style=\"width: 80px;\" readonly/>";
                //    OfferPrice = "<input type=\"text\" disabled=\"disabled\" value=\"" + msg[i].Offer_Price + "\" id=\"txtOfferPrice_" + i + "\" onkeypress=\"return isNumberKey(event,this);\" style=\"width: 80px;\" readonly/>";
                //    NewOfferPrice = "<input type=\"text\" disabled=\"disabled\" value=\"" + msg[i].New_OfferPrice + "\" id=\"txtNewOfferPrice_" + i + "\" onkeypress=\"return isNumberKey(event,this);\" style=\"width: 80px;\" readonly/>";
                //    comment = "<textarea  disabled=\"disabled\" id=\"txtComment_" + i + "\" cols=\"20\" rows=\"2\" readonly></textarea><br/><span id=\"lblmsg_" + i + "\" style=\"color:red;\"></span>";
                //    //expiry = "<input type=\"text\" id=\"txtExpiryDate_" + i + "\" disabled=\"disabled\"><input type=\"hidden\" id=\"hdnExpiryDate_" + i + "\" value=\"" + msg[i].ExpiryDate + "\">"
                //}
                    
                button = '<img src="images/icons/view_details.png" style="height:25px;" title="View Details" id="imgView_' + i + '" onclick="OpenDetailView(this);" /><input type="hidden" id="hdnID_' + i + '" value="' + msg[i].ID+ '">';
                                   
                                    $("#grdDetailedQuoteSummary tbody ").append(" <tr>  <td>" +
                                        msg[i].CP_Name + "</td>  <td>" +
                    msg[i].Item_code + "</td>  <td>" +
                    msg[i].Item_Desc + "</td>  <td>" +
                    //msg[i].WHS + "</td>  <td>" +
                    //msg[i].Order_type + "</td>  <td>" +
                    //msg[i].Order_frequency + "</td>  <td>" +
                    //msg[i].Total_QTY + "</td>  <td>" +
                    //msg[i].QTY_perOrder + "</td>  <td>" +
                    //msg[i].List_Price + "</td>  <td>" +
                    msg[i].Expected_price + "</td>  <td>" +
                    msg[i].Order_Validity + "</td>  <td>" +
                    msg[i].Approved_OrderQty + "</td>  <td>" +
                   // msg[i].DC_rate + "</td>  <td>" +
                    //msg[i].Cust_number + "</td>  <td>" +
                    //msg[i].Cust_Name + "</td>  <td>" +
                    //msg[i].Cust_SP + "</td>  <td>" +
                    //msg[i].Comp_Name + "</td>  <td>" +
                    //msg[i].Comp_Desc + "</td>  <td>" +
                    //msg[i].Comp_SP + "</td>  <td>" +
                    msg[i].MOQ + "</td>  <td>" +
                    msg[i].Offer_Price + "</td>  <td>" +
                    msg[i].New_OfferPrice + "</td>  <td>" +
                   // expiry + "</td>  <td>" +
                    msg[i].Status + "</td>  <td>" +
                    
                    button +
                    "</td>  </tr>");
                // alert(JSON.stringify(msg));
            }
            debugger;
            //var totaltrCount = $("[id*=grdDetailedQuoteSummary] tr").length;
            //var trCount = $("[id*=grdDetailedQuoteSummary] td").closest("tr").length;
            //var date_id, hdn_date_id;
            //for (i = 0; i < trCount; i++) {
            //    date_id = "txtExpiryDate_" + i;
            //    hdn_date_id = "hdnExpiryDate_" + i;
            //    var defDate = $('#' + hdn_date_id).val();
            //    debugger;
            //    $('#' + date_id).daterangepicker({
            //        singleDatePicker: true,
            //        startDate: defDate,
            //        endDate: null,
            //        locale: {
            //            format: 'MM/DD/YYYY'
            //        },
            //        showDropdowns: true,
            //    }, function (start, end, label) {
            //        debugger;
            //        var id = this.element[0].id;
            //        var hdnid = this.element[0].id.replace("txtExpiryDate", "hdnExpiryDate");
            //        $('#' + hdnid).val(start.format('MM/DD/YYYY'));
            //        // LoadExpiryDate(hdnid, id, start.format('MM/DD/YYYY'));
            //        var name = this.element[0].name;
            //        $('body').find("input[name^='" + name + "']").val(start.format('MM/DD/YYYY'));
            //    });

            //}
            debugger;
            if ($.fn.dataTable.isDataTable('#grdDetailedQuoteSummary')) {
                //$('#grdDetailedQuoteSummary').DataTable().destroy();
                //table1.destroy();
                //table1 = $('#grdDetailedQuoteSummary').DataTable({
                //    //destroy: true,
                //});
            }
            else {
                //table1.destroy();
                table1 = $('#grdDetailedQuoteSummary').DataTable({
                    scrollX: true,
                    scrollCollapse: true,
                    //fixedColumns: true,
                    //fixedColumns: {
                    //    leftColumns: 1,
                    //    rightColumns: 4
                    //},
                });
            }

        }

        function OpenDetailView(obj)
        {
            var flag = 'Summary';
            var id = obj.id;
            var ref_id = id.replace("imgView", "hdnID");
            var ref = $("#" + ref_id).val();
            win = window.open("QuoteSummaryDetails.aspx?id=" + ref + "&flag=" + flag, "_blank", "WIDTH=900,HEIGHT=650,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
        }
        function Approve(Id, item, ref, rowcount, status) {
            debugger;
            //var expiry_date =$("#txtExpiryDate_" + rowcount).val();
            var comment_id = "txtComment_" + rowcount;
            var comment = $("#" + comment_id).val();
            var MOQ = $("#txtMOQ_" + rowcount).val();
            var offerprice = $("#txtNewOfferPrice_" + rowcount).val();
            var lbl = "lblpricemsg_" + rowcount;
            if (status == 'Rejected') {
                if (IsnullOrEmpty(offerprice)) {
                    offerprice = "";
                }
            }
            var multiflag = "1";
            if (status == 'Approved' && IsnullOrEmpty(offerprice)) {
                $("#txtNewOfferPrice_" + rowcount).css("border", "1px solid red");
                $("span#" + lbl).text("*Required");
            }
            else {
                $("span#" + lbl).text("");
                var objList = [];
                var obj = {
                    "id": Id
                    , "item": item
                    , "ref_no": ref
                    , "status": status
                    , "comment": comment
                    , "MOQ": MOQ
                    , "offerPrice": offerprice
                    , "multiorder_flag": multiflag
                    // , "expiry_date": expiry_date
                };
                objList.push(obj);
                var param = "{objList:" + JSON.stringify(objList) + "}";
                $.ajax({
                    url: 'PendingTasks.aspx/UpdateQuoteStatus',
                    method: 'post',
                    datatype: 'json',
                    data: param,
                    //data: '{id:"' + Id + '", item:"' + item + '", ref_no:"' + ref + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:"' + multiflag + '"}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        window.location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.responseText);
                    }
                });
            }
        }

        function Reject(Id, item, ref, rowcount) {
            debugger;
            var errFlag = 0;
            var comment_id = "txtComment_" + rowcount;
            var lbl = "lblmsg_" + rowcount;
            if (IsnullOrEmpty($("#" + comment_id).val())) {
                $("#" + comment_id).css("border", "1px solid red");
                $("span#" + lbl).text("*Required");
                errFlag++;
            }
            else {
                $("span#" + lbl).text("");
                $("#" + comment_id).css("border", "");
            }

            if (errFlag > 0) {
                return false;
            }
            else {
                Approve(Id, item, ref, rowcount, "Rejected");
            }
        }
        function validateFields(evt, obj) {
            debugger;
            var errFlag = 0;
            var id = obj.id;
            var comment_id = id.replace("btnReject", "txtComment");
            var lbl = id.replace("btnReject", "lblmsg");
            if (IsnullOrEmpty($("#" + comment_id).val())) {
                $("#" + comment_id).css("border", "1px solid red");
                $("span#" + lbl).text("*Required");
                errFlag++;
            }
            else {
                $("span#" + lbl).text("");
                $("#" + comment_id).css("border", "");
            }

            if (errFlag > 0) {
                return false;
            }
            else {
                return true;
            }
        }
        function IsnullOrEmpty(val) {
            if (val != '' && val != undefined && val != '--Select--')
                return false;
            else
                return true;
        }
        function setTextareaVal(evt, obj) {
            debugger;
            var value = obj.value;
            $("#" + obj.id).val(value);
        }
        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;
                if (charCode == 46) return false;
            }
            else {
                if (value.length > 10) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function Clear()
        {
            sessionStorage.removeItem('selectedSummaryStart');
            sessionStorage.removeItem('selectedSummaryEnd');
            var SelectedStart = sessionStorage.getItem("selectedSummaryStart");
            var SelectedEnd = sessionStorage.getItem("selectedSummaryEnd");
            var start = (SelectedStart == null ? "07/20/2019" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#body_txtDateRange').daterangepicker({
                //opens: 'left',
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedSummaryStart', start);
                sessionStorage.setItem('selectedSummaryEnd', end);
            });
            $("#body_ddlStatus").val("ALL");
            $("#body_ddlcustType").val("ALL");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="container1">
        <div class="col-md-12">
            <div class="mn_view_stock">
                <h4>Quote Summary </h4>
            </div>
            <div class="mn_section mn_filter">
                <div>
                    <div>
                        <div class="col-md-2 mn_view_stock pull-right">
                            <label class="control-label">Requested Date</label>
                            <asp:TextBox ID="txtDateRange" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-2 mn_view_stock pull-right">
                            <label class="control-label">Customer Type</label>
                            <asp:DropDownList ID="ddlcustType" runat="server">
                                <asp:ListItem Text="ALL" Value="ALL"></asp:ListItem>
                                <asp:ListItem Text="Direct Customers" Value="Customer"></asp:ListItem>
                                <asp:ListItem Text="Channel Partner" Value="Distributor"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                         <div class="col-md-2 mn_view_stock pull-right">
                            <label class="control-label">Status</label>
                            <asp:DropDownList ID="ddlStatus" runat="server">
                                <asp:ListItem Text="ALL" Value="ALL"></asp:ListItem>
                                <asp:ListItem Text="Open" Value="Open"></asp:ListItem>
                                <asp:ListItem Text="Close" Value="Close"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-1 mn_view_stock" style="margin-top: 30px;">

                            <asp:Button ID="btnFilter" runat="server" CssClass="btn btn-success button pull-right" Text="Filter" OnClick="btnFilter_Click" />
                        </div>
                        <div class="col-md-1 mn_view_stock" style="margin-top: 30px;">
                            <asp:Button ID="btnClear" runat="server" CssClass="btn btn-success button" Text="Clear" OnClientClick="Clear();" OnClick="btnFilter_Click" />
                        </div>
                        <div class="col-md-4 mn_view_stock pull-right"></div>
                    </div>
                </div>
            </div>
            <asp:Label ID="lblresult" Text="" runat="server"></asp:Label>
            <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
            <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
                <%--<asp:Panel runat="server" ID="pnlData">--%>
                <ContentTemplate>
                    <asp:GridView ID="grdQuoteSummary" CssClass="display compact" runat="server" Width="100%" AutoGenerateColumns="false">
                        <Columns>

                            <asp:TemplateField HeaderText="Reference No">
                                <ItemTemplate>
                                    <asp:Label ID="lblref" CssClass="link" runat="server" Text='<%#Bind("Ref_number") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblCust" runat="server" Text='<%#Bind("cust_type") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer/Channel Partner Number">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustCPNum" runat="server" Text='<%#Bind("CustCP_Num") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer/Channel Partner Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustCPName" runat="server" Text='<%#Bind("CustCP_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order Value">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrder" Style="float: right;" runat="server" Text='<%#Bind("Order_value") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Requested Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%#Bind("Requested_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" HeaderStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:Label ID="lblstatus" runat="server" Text='<%#Bind("Status") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <div id="divdetail" style="display: none;">
                        <table id="grdDetailedQuoteSummary" class="display responsive nowrap" cellpadding="0" cellspacing="0" style="width:100%;">
                            <thead style="background-color: #DC5807; color: White; font-weight: bold">
                                <tr style="border: solid 1px #000000">
                                    <td>CP Name</td>
                                    <td>Item_code</td>
                                    <td>Item_Desc</td>
                                    <td>Expected Price</td>
                                    <td>Order Valildity</td>
                                    <td>Break Quantity</td>
                                     <td>MOQ</td>
                                    <td>System Price</td>
                                    <td>Special Price</td>
                                    <td>Status</td>
                                    <td>Action</td>
                                     <%-- <td>WHS</td>
                                    <td>Order Type</td>
                                    <td>Order Frequency</td>
                                    <td>Total Qty</td>
                                    <td>QTY Per Order</td>
                                    <td>List Price</td>
                                    <td>Discount Rate(%)</td>
                                    <td>Cust Name</td>
                                    <td>Sales Price</td>
                                    <td>Competitor Company</td>
                                    <td>Competitor Product</td>
                                    <td>Competitor Sales Price</td>
                                    <td>Expiry Date</td>--%>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

    </div>
</asp:Content>
