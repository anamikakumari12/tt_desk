﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBO;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Configuration;
using OtpNet;

namespace TT_Desk
{
    public partial class OTP : System.Web.UI.Page
    {
        #region GlobalDeclarations
        CommonFunctions objCom = new CommonFunctions();
        AuthClass authObj = new AuthClass();
        #endregion

        #region Events

        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// Desc: On Click of submit button validate both, Entered OTP and OTP sent by mail and logon to application
        /// </summary>
        /// <returns></returns>
        protected void BtnOtp_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(Convert.ToString(Session["Failurecount"])))
                {
                    Session["Failurecount"] = 5;
                }
                else
                {
                    Session["Failurecount"] = Convert.ToInt32(Session["Failurecount"]) - 1;
                    lblOtpFailure.Text = "you are left with only " + Convert.ToInt32(Session["Failurecount"]) + "Attempt(s)";
                }

                if (Convert.ToInt32(Session["Failurecount"]) > 0)
                {
                    if (!String.IsNullOrEmpty(Convert.ToString(Session["otp"])))
                    {
                        if (Convert.ToString(txtotp.Text) == Convert.ToString(Session["otp"]))
                        {
                            Response.Redirect("PendingTasks.aspx");
                        }
                        else
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "OTPalert", "alert('Invalid Otp'); showModal()", true);
                        }
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "OTPalert", "alert('OTP is timed out. please regenerate new OTP by clicking on Resend OTP link')", true);
                        lblOtpFailure.Text = "Your maximum attempts are exceeded";
                    }
                }
                else
                {
                    lblOtpFailure.Text = "";
                    Session["Failurecount"] = 5;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "OTPalert", "alert('Your maximum attempt(s) exceeded, please regenerate new OTP by clicking on Resend OTP link.')", true);
                    txtotp.Text = string.Empty;
                    Session["otp"] = null;
                    // Response.Redirect("Login.aspx");
                }

                //}
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                lblOtpFailure.Text = "";
                txtotp.Text = string.Empty;
                Session["otp"] = null;
            }
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// Desc: On Click of Reset OTP, new OTP will generate and send to respective mailid
        /// </summary>
        /// <returns></returns>
        protected void ResendOtp_Click(object sender, EventArgs e)
        {
            string StrOtp = null;
            string loginmailid = null;
            try
            {
                //txtotp.Text = string.Empty;
                lblOtpFailure.Text = string.Empty;
                Session["otp"] = null;
                if (authObj.ErrorNum == 0)
                {
                    StrOtp = GenerateOtp();
                    if (String.IsNullOrEmpty(StrOtp))
                    {
                        lblOtpFailure.Text = "Something went wrong please try again";
                    }
                    else
                    {
                        authObj = Session["authObj"] as AuthClass;
                        loginmailid = authObj.LoginMailID;
                        SendOtpToMail(StrOtp, loginmailid);
                    }
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                StrOtp = null;
                loginmailid = null;
            }
        }
        #endregion

        #region methods
        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// Desc: To generate new OTP
        /// </summary>
        /// <returns>it will return OTP as a string</returns>
        public string GenerateOtp()
        {
            string Otp = null;
            try
            {
                byte[] b = new byte[] { Convert.ToByte(20), Convert.ToByte('A'), Convert.ToByte('T') };
                var otp = new Totp(b, mode: OtpHashMode.Sha512, step: 5);
                Otp = otp.ComputeTotp(DateTime.UtcNow);
                Session["otp"] = Otp;
                return Otp;
            }
            catch (Exception ex)
            {
                //CommonFunctions.LogErrorStatic(ex);
                return null;
            }
            finally
            {
                Otp = null;
            }
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// Desc: It will send the OTP to mail
        /// </summary>
        /// <returns></returns>
        private void SendOtpToMail(string StrOtp, string loginmailId)
        {
            CommonFunctions comobj = new CommonFunctions();
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            EmailDetails objEmail;
            try
            {
                objEmail = new EmailDetails();
                objEmail.body = "<br/><br/>" + StrOtp + Convert.ToString(ConfigurationManager.AppSettings["OtpMessage"]) + "<br/><br/>";
                objEmail.toMailId = loginmailId;
                objEmail.subject ="tt_Desk Login OTP";//Subject for your request

                comobj.SendMail(objEmail);
            }
            catch (Exception ex)
            {
                comobj.ErrorLog(ex);
            }
            finally
            {
                objEmail = new EmailDetails();
            }

        }


        #endregion


    }
}