﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBO;

namespace TT_Desk
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        CommonFunctions objCom = new CommonFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblUserName.Text = Convert.ToString(Session["UserFullName"]);
            }
        }

        protected void lnkbtnLogout_Click(object sender, EventArgs e)
        {
            if (Session["UserFullName"] != null)
            {

                Session["LoginMailId"] = null;
                Session["EscalatedQuotes"] = null;
                Session["UserFullName"] = null;
                Session["UserId"] = null;
                Session["PendingQuotes"] = null;
                Session["AcceptedQuotes"] = null;
                Session["dtQuoteDetails"] = null;

                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("Login.aspx");

                //ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }

        

       
    }
}