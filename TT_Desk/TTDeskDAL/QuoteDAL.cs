﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using TTDeskBO;

namespace TTDeskDAL
{
    public class QuoteDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable GetPendingTasksDAL( QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getPendingQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlcmd.Parameters.Add(ResourceDAL.role, SqlDbType.VarChar, 10).Value = ResourceDAL.Role_flag;
                sqlcmd.Parameters.Add(ResourceDAL.cter, SqlDbType.VarChar, 10).Value = null;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public DataTable GetEscalatedTasksDAL(QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getEscalatedQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlcmd.Parameters.Add(ResourceDAL.role, SqlDbType.VarChar, 10).Value = "HO";
                sqlcmd.Parameters.Add(ResourceDAL.cter, SqlDbType.VarChar, 10).Value = null;
                sqlcmd.Parameters.Add(ResourceDAL.assigned_salesengineer_id, SqlDbType.VarChar, 10).Value = null;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public List<QuoteBO> updateQuoteStatusDAL(DataTable dtQuote)
        {
            List<QuoteBO> objList = new List<QuoteBO>();
            QuoteBO objOutput = new QuoteBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataSet ds = new DataSet();
            DataTable dtoutput = new DataTable();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_quoteStatusChange, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceDAL.tblQuote, dtQuote);
                //sqlcmd.Parameters.Add(ResourceDAL.pdf_flag, SqlDbType.Int).Direction = ParameterDirection.Output;
                //sqlcmd.Parameters.Add(ResourceDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                //sqlcmd.Parameters.Add(ResourceDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                
                //sqlcmd.ExecuteNonQuery();
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(ds);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ds.Tables[0].Columns.Remove("Item");
                            ds.Tables[0].AcceptChanges();
                            DataView view = new DataView(ds.Tables[0]);
                            string[] param = { "Ref_Number", "Quotation_no", "MailTo", "MAilCC", "Subject", "Body", "error_code", "error_msg", "PDF_Flag" };
                            DataTable distinctValues = view.ToTable(true,param);

                            var row = distinctValues.AsEnumerable()
                                .Where(o => o.Field<string>("MailTo")!= null || o.Field<string>("MailTo")!="")
                                .Select(o => new QuoteBO { Ref_Number = o.Field<string>("Ref_Number")
                                    , Quotation_no=o.Field<string>("Quotation_no")
                                    , to=o.Field<string>("MailTo")
                                    , cc=o.Field<string>("MAilCC")
                                    , subject=o.Field<string>("Subject")
                                    , message=o.Field<string>("Body")
                                    , Error_code=o.Field<Int32>("error_code")
                                    , Error_msg = o.Field<string>("error_msg")
                                    , pdf_flag=o.Field<Int32>("PDF_Flag")
                                }).Distinct().ToList();
                            objList = row;

                            //string[] param_pdf = { "Ref_Number", "Quotation_no", "Item" };
                            //DataTable distinctValues_pdf = view.ToTable(true, param_pdf);
                           
                            //foreach(QuoteBO obj in objList)
                            //{
                            // var row_pdf= distinctValues.AsEnumerable()
                            //    .Where(o => o.Field<string>("Ref_Number") != obj.Ref_Number)
                            //    .Select(o => new pdf_detail
                            //    {
                            //        Ref_Number = o.Field<string>("Ref_Number")
                            //        , Quotation_no=o.Field<string>("Quotation_no")
                            //        , Item_Number=o.Field<string>("Item")
                                    
                            //    }).Distinct().ToList();
                            //    obj.pdf_detail = row_pdf;
                            //}

                        }
                    }
                }
                //objList = ConvertDataTable<QuoteBO>(dtoutput);
                //objOutput.pdf_flag = Convert.ToInt32(sqlcmd.Parameters[ResourceDAL.pdf_flag].Value);
                //objOutput.Error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceDAL.error_code].Value);
                //objOutput.Error_msg = Convert.ToString(sqlcmd.Parameters[ResourceDAL.error_msg].Value);
                
                //sqlcmd = new SqlCommand(ResourceDAL.sp_getMailDetailsForQuote, sqlconn);
                //sqlcmd.CommandType = CommandType.StoredProcedure;
                //sqlcmd.Parameters.Add(ResourceDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                //sqlcmd.Parameters.Add(ResourceDAL.Component, SqlDbType.VarChar, -1).Value = "StatusChange";
                //sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                //sqlcmd.Parameters.Add(ResourceDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                //sqlda = new SqlDataAdapter(sqlcmd);
                //sqlda.Fill(dtoutput);

                //if (dtoutput != null)
                //{
                //    if (dtoutput.Rows.Count > 0)
                //    {
                //        objOutput = new QuoteBO();
                //        objOutput.to = Convert.ToString(dtoutput.Rows[0]["MailTo"]);
                //        objOutput.cc = Convert.ToString(dtoutput.Rows[0]["MailCC"]);
                //        objOutput.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
                //        objOutput.message = Convert.ToString(dtoutput.Rows[0]["Body"]);
                //        objList.Add(objOutput);
                //    }
                //}
            }
            catch (Exception ex)
            {
                objOutput.Error_code = 1;
                objOutput.Error_msg = "There is some error in status update. Please try again.";
                objList.Add(objOutput);
                objCom.ErrorLog(ex);
            }
            return objList;
        }

        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        } 

        public DataTable GetQuoteSummaryDAL(QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getQuoteSummaryDesk, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlcmd.Parameters.Add(ResourceDAL.flag, SqlDbType.VarChar, 100).Value = objQuoteBO.flag;
                sqlcmd.Parameters.Add(ResourceDAL.Status, SqlDbType.VarChar, 100).Value = objQuoteBO.Status;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public DataTable GetQuoteDetailsDAL(QuoteBO objquoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            //QuoteBO objquoteBO = new QuoteBO();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getQuoteDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.ID, SqlDbType.Int, 100).Value = objquoteBO.QuoteID;
                sqlcmd.Parameters.Add(ResourceDAL.item, SqlDbType.VarChar, 100).Value = objquoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, 100).Value = objquoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceDAL.Status, SqlDbType.VarChar, 100).Value = objquoteBO.Status;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public DataTable getQuoteFormatDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getQuoteFormat, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceDAL.Quotation_no, SqlDbType.VarChar, -1).Value = objQuoteBO.Quotation_no;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetAcceptedQuotesDAL(QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getAcceptedQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

       
    }
}