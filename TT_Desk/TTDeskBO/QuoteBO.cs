﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTDeskBO
{
    public class QuoteBO
    {
        public int ID { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public string ChangeBy { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string MOQ { get; set; }
        public string Offer_Price { get; set; }
        public int Error_code { get; set; }
        public string Error_msg { get; set; }
        public int pdf_flag { get; set; }
        public string flag { get; set; }

        public int QuoteID { get; set; }

        public string Item_Number { get; set; }
        public string Quotation_no { get; set; }
        public string Ref_Number { get; set; }
        public string to { get; set; }
        public string cc { get; set; }
        public string subject { get; set; }
        public string message { get; set; }

        public int MultiOrderFlag { get; set; }

    }

    public class QuoteStatus
    {
        public string id { get; set; }
        public string item { get; set; }
        public string ref_no { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
        public string MOQ { get; set; }
        public string offerPrice { get; set; }
        public string multiorder_flag { get; set; }
        //public string expiry_date { get; set; }
        public string Order_Validity { get; set; }
        public string Approved_OrderQty { get; set; }
        public string Approved_OrderFreq { get; set; }
    }

    public class pdf_detail
    {
        public string Item_Number { get; set; }
        public string Quotation_no { get; set; }
        public string Ref_Number { get; set; }
    }
    public class QuoteStatusDB
    {
        public int ID { get; set; }
        public string RefNumber { get; set; }
        public string item { get; set; }
        public string flag { get; set; }
        public string changeby { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public string MOQ { get; set; }
        public string OfferPrice { get; set; }
        public int MultiOrderFlag { get; set; }
        public string RecommendedPrice { get; set; }
        //public DateTime expiry_date { get; set; }
        public string Order_Validity { get; set; }
        public string Approved_OrderQty { get; set; }
        public string Approved_OrderFreq { get; set; }
        public string Comp_name { get; set; }
        public string Comp_desc { get; set; }
        public string Comp_SP { get; set; }
    }
}