﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Data;

namespace TTDeskBO
{
    public class CommonFunctions
    {
        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Nov 2, 2020
        /// Desc : Store the exception message in a file 
        /// </summary>
        /// <param name="ex">Exception</param>
        public void ErrorLog(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            string ErrorLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
            string ErrorFile = ConfigurationManager.AppSettings["ErrorFile"].ToString();
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Nov 2, 2020
        /// Desc : Encrypt the text with AES algorithm using encription key
        /// </summary>
        /// <param name="clearText"></param>
        /// <returns></returns>
        public string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return clearText;
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Nov 2, 2020
        /// Desc : The Method is used to send mail using SMTP server with detail stored in web config file.
        /// </summary>
        /// <param name="objEmail"></param>
        public void SendMail(EmailDetails objEmail)
        {
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            MailMessage email;
            SmtpClient smtpc;
            try
            {
                email = new MailMessage();
                if (objEmail.toMailId.Contains(";"))
                {
                    List<string> names = objEmail.toMailId.Split(';').ToList<string>();
                    for (int i = 0; i < names.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                            email.To.Add(names[i]);
                    }
                }
                else
                {
                    email.To.Add(objEmail.toMailId);
                }

                // email.To.Add(objEmail.toMailId); //Destination Recipient e-mail address.
                if (!string.IsNullOrEmpty(objEmail.ccMailId))
                {
                    if (!string.IsNullOrEmpty(objEmail.ccMailId))
                    {
                        if (objEmail.ccMailId.Contains(";"))
                        {
                            List<string> names = objEmail.ccMailId.Split(';').ToList<string>();
                            for (int i = 0; i < names.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                    email.CC.Add(names[i]);
                            }
                        }
                        else
                        {
                            email.CC.Add(objEmail.ccMailId);
                        }
                    }
                }
                   // email.CC.Add(objEmail.ccMailId);
                email.Subject = objEmail.subject;
                email.Body = objEmail.body;
                email.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(objEmail.attachment))
                    email.Attachments.Add(new Attachment(objEmail.attachment));
                smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                email = new MailMessage();
            }
        }
        
    }
}