﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TTDeskDAL;
using TTDeskBO;

namespace TTDeskBL
{
    public class QuoteBL
    {
        QuoteDAL objQuoteDAL = new QuoteDAL();
        public DataTable GetPendingTasksBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetPendingTasksDAL(objQuoteBO);
        }

        public List<QuoteBO> updateQuoteStatusBL(DataTable dtQuote)
        {
            return objQuoteDAL.updateQuoteStatusDAL(dtQuote);
        }

        public DataTable GetQuoteSummaryBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetQuoteSummaryDAL(objQuoteBO);
        }

        public DataTable GetQuoteDetailsBL(QuoteBO objquoteBO)
        {
            return objQuoteDAL.GetQuoteDetailsDAL(objquoteBO);
        }

        public DataTable getQuoteFormatBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.getQuoteFormatDAL(objQuoteBO);
        }

        public DataTable GetAcceptedQuotesBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetAcceptedQuotesDAL(objQuoteBO);
        }

        public DataTable GetEscalatedTasksBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetEscalatedTasksDAL(objQuoteBO);
        }

       
    }
}