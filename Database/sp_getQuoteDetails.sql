USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getQuoteDetails]    Script Date: 7/31/2019 12:46:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_getQuoteDetails]
(
@ID INT=null,
@item VARCHAR(100)=null,
@RefNumber VARCHAR(100)=null
)
As
BEGIN

SELECT * FROM Quote_ref WHERE Item_code =ISNULL(@item, Item_code) AND Ref_number=ISNULL(@RefNumber,Ref_Number) AND ID=ISNULL(@ID,ID)

END

