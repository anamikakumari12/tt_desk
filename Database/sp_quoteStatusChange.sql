USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_quoteStatusChange]    Script Date: 7/31/2019 11:55:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE  [dbo].[sp_quoteStatusChange]
(
@changeby VARCHAR(MAX),
@Status VARCHAR(100),
@Reason VARCHAR(MAX)=null,
@MOQ VARCHAR(100)=null,
@OfferPrice VARCHAR(100)=null,
@ID INT
)

AS
BEGIN

	UPDATE Quote_ref SET Status=@Status
	, Approval_date= CASE  WHEN @Status='Approved' THEN GETDATE() ELSE NULL END
	, Rejected_date= CASE  WHEN @Status='Rejected' THEN GETDATE() ELSE NULL END
	, Rejected_Reason=CASE  WHEN @Status='Rejected' THEN @Reason ELSE NULL END
	, Approved_Rejected_By=@changeby
	, MOQ=@MOQ
	,Offer_Price=@OfferPrice
	WHERE ID=@ID
END


ALTER TABLE Quote_ref ADD MOQ VARCHAR(100)
ALTER TABLE Quote_ref ADD Offer_Price VARCHAR(100)