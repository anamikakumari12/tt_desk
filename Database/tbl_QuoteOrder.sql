DROP TABLE tbl_QuoteOrder
CREATE TABLE tbl_QuoteOrder
(
ID INT IDENTITY(1,1),
Quote_Id INT,
Ref_number		VARCHAR(200),
Item			VARCHAR(100),
Order_Type		VARCHAR(100),
Schedule_Type	VARCHAR(100),
NumberofOrder	VARCHAR(100),
QuantityPerOrder	VARCHAR(100),
OrderBy_ID		VARCHAR(100),
OrderBY_Name	VARCHAR(MAX),
OrderBy_CP_SE	VARCHAR(100),
OrderPlace_Date	DateTime,
Order_Status	VARCHAR(100)	
)

ALTER PROCEDURE sp_PlaceOrderForQuote
(
@QuoteId	INT,
@RefNumber	VARCHAR(200),
@item		VARCHAR(100),
@OrderType	VARCHAR(100),
@ScheduleType	VARCHAR(100),
@Quantity		VARCHAR(100),
@OrderByID	VARCHAR(100),
@OrderByName	VARCHAR(100),
@OrderByCPSE		VARCHAR(100),
@NumberOfOrder		VARCHAR(100),
@error_code		INT OUTPUT,
@error_msg		VARCHAR(MAX) OUTPUT
)
AS
BEGIN
	IF(@QuoteId=0 OR @QuoteId is null)
	BEGIN
	SET @error_code=100
	SET @error_msg='Error: Quote ID is required.'
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@QuoteId)
		BEGIN
			IF EXISTS(SELECT * FROM Quote_ref WHERE ID=@QuoteId AND Ref_number=@RefNumber AND Item_code=@item)
			BEGIN
				
				INSERT INTO tbl_quoteOrder(Quote_Id,Ref_number, Item, Order_Type, Schedule_Type,NumberofOrder,QuantityPerOrder, OrderBy_ID, OrderBY_Name, OrderBy_CP_SE, OrderPlace_Date,Order_Status)
				VALUES(@QuoteId,@RefNumber,@item,@OrderType,@ScheduleType, @NumberOfOrder,@Quantity, @OrderByID,@OrderByName,@OrderByCPSE,GETDATE(),'Placed')
				IF EXISTS(SELECT * FROM tbl_quoteOrder WHERE Quote_Id=@QuoteId AND Ref_number=@RefNumber AND Item=@item)
				BEGIN
					SET @error_code=200
					SET @error_msg='Order is placed successfully.'
				END
				ELSE
				BEGIN
					SET @error_code=103
					SET @error_msg='Error: Error in saving data.'
				END
			END											
			ELSE
			BEGIN
				SET @error_code=102
				SET @error_msg='Error: Reference Number and Item code are not available in Quote table.'
			END
		END
		ELSE
		BEGIN
			SET @error_code=101
			SET @error_msg='Error: Quote ID is not available in Quote table.'
		END
	END

END